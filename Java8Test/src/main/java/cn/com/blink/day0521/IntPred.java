package cn.com.blink.day0521;

/**
 * Created by liuzh on 2017/5/21.
 */
public interface IntPred {
    boolean test(Integer value);
}
