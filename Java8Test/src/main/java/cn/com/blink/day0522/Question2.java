package cn.com.blink.day0522;


import cn.com.blink.bean.Artist;
import cn.com.blink.bean.SampleData;
import org.junit.Test;

import java.util.List;

public class Question2 {
    // Q3
    public static int countBandMembersInternal(List<Artist> artists) {
        // NB: readers haven't learnt about primitives yet, so can't use the sum() method
        return artists.stream()
                .map(artist -> artist.getMembers().count())
                .reduce(0L, Long::sum)
                .intValue();
    }

    @Test
    public void test() {
        System.out.println(countBandMembersInternal(SampleData.getThreeArtists()));
    }
}
