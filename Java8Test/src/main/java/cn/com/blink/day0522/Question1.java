package cn.com.blink.day0522;


import cn.com.blink.bean.Album;
import cn.com.blink.bean.Artist;
import cn.com.blink.bean.SampleData;
import org.junit.Test;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Question1 {
    public static int addUp(Stream<Integer> numbers) {
        return numbers.reduce(0, (acc, x) -> acc + x);
    }

    public static List<String> getNamesAndOrigins(List<Artist> artists) {
        return artists.stream()
                .flatMap(artist -> Stream.of(artist.getName(), artist.getNationality()))
                .collect(toList());
    }

    public static List<Album> getAlbumsWithAtMostThreeTracks(List<Album> input) {
        return input.stream()
                .filter(album -> album.getTrackList().size() <= 3)
                .collect(toList());
    }


    @Test
    public void test() {
        System.out.println(addUp(Stream.iterate(0, x -> x + 1).limit(100)));

        getNamesAndOrigins(SampleData.getThreeArtists()).forEach(System.out::println);

        getAlbumsWithAtMostThreeTracks(SampleData.getAlbums());
    }
}
