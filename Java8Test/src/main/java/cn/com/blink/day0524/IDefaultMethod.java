package cn.com.blink.day0524;

//import java.util.Objects;

/**
 * 因为hashcode和equals是Object类中已经实现的方法，
 * 因此接口中不能通过默认方法的方式覆盖这俩方法
 * Created by liuxianzhao on 2017/5/24.
 */
public interface IDefaultMethod {
//    public default int hashCode() {
//        return Objects.hashCode(this);
//    }
//
//    public default boolean equals(Object obj) {
//        return (this == obj);
//    }
}
